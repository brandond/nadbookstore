﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using BookStore.Models;

namespace BookStore
{
    public partial class ManageInstitutions : Page
    {
        public class UserFaculty
        {
            public String Name { get; set; }
            public int ID { get; set; }
            public String Courses { get; set; }//should be dropdown list


            public UserFaculty()
            {
            }

            public UserFaculty(string _name, int _ID, string _Courses)
            {
                Name = _name;
                ID = _ID;
                Courses = _Courses;
            }
        }

        List<UserFaculty> customerList;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
                var requestURL = baseURL + @"/api/Users/Login";
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                request.Method = "GET";


                var response = (HttpWebResponse)request.GetResponse();

                User user = null;

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                        if (user.accessLevel < 3)
                        {
                            Response.Redirect("~/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Account/Login");
            }



            customerList = new List<UserFaculty>();

            UserFaculty co1 = new UserFaculty { Name = "Donna", ID = 40, Courses = "SET" };
            UserFaculty co2 = new UserFaculty("Raj", 10, "CET");
            UserFaculty co3 = new UserFaculty("Art", 16, "SET");
            customerList.Add(co1);
            customerList.Add(co2);
            customerList.Add(co3);
            testDataGrid.DataSource = customerList;
            testDataGrid.DataBind();
        }

        void addbuttons()
        {
            List<Button> editButtons = new List<Button>();
            List<Button> deleteButtons = new List<Button>();


            Button editBTN = new Button();
            editBTN.Text = "EDIT";
            editBTN.CssClass = "btn btn-sm btn-info";
            editBTN.Attributes.Add("EditBtnID", "userIDNumber");
            editBTN.Click += new EventHandler(editBTN_Click);
            editButtons.Add(editBTN);
            //  Setting up the Delete button and all that              

            Button deleteBTN = new Button();
            deleteBTN.CssClass = "btn btn-sm btn-danger";
            deleteBTN.Text = "DELETE";
            deleteBTN.Attributes.Add("DeleteBtnID", "userIDNumber");
            deleteBTN.Attributes.Add("onclick", "return window.confirm('Are you sure you want to delete user?');");
            //deleteBTN.Attributes.Add("DeleteBtnuName", EI.uName);
            deleteBTN.Click += new EventHandler(deleteBTN_Click);
            deleteButtons.Add(deleteBTN);
        }

        private void deleteBTN_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            string empID = button.Attributes["DeleteBtnID"];
            //string empUName = button.Attributes["DeleteBtnuName"];
            Response.Redirect(Request.RawUrl);
            //throw new NotImplementedException();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Event handler. Called by editBTN for click events. </summary>
        ///
        /// <remarks>   Wesley, 2017-04-17. </remarks>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        ///-------------------------------------------------------------------------------------------------

        private void editBTN_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            string empID = button.Attributes["EditBtnID"];
            Session["empID"] = empID;
            string perID = button.Attributes["EditBtnPerID"];
            Session["perID"] = perID;
            Response.Redirect("~/Views/Edit/editEmployee.aspx", true);
        }
    }
}