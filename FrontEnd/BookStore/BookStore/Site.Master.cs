﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Net;
using BookStore.Models;
using System.IO;
using Newtonsoft.Json;

namespace BookStore
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            browse.Visible = false;
            myListings.Visible = false;
            manageUsers.Visible = false;
            manageBooks.Visible = false;
            manageCourses.Visible = false;
            manageInits.Visible = false;
            try
            {
                string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
                var requestURL = baseURL + @"/api/Users/Login";
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                request.Method = "GET";


                var response = (HttpWebResponse)request.GetResponse();

                User user = null;

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                        if (user.accessLevel < 3)
                        {
                            browse.Visible = true;
                            myListings.Visible = true;
                            manageUsers.Visible = true;
                            manageBooks.Visible = true;
                            manageCourses.Visible = true;

                            manageInits.Visible = false;
                            if (user.accessLevel < 2)//user is a student
                            {
                                browse.Visible = true;
                                myListings.Visible = true;

                                manageUsers.Visible = false;
                                manageBooks.Visible = false;
                                manageCourses.Visible = false;
                                manageInits.Visible = false;

                            }
                        }
                        else//system admin
                        {
                            //all visible
                            browse.Visible = true;
                            myListings.Visible = true;
                            manageUsers.Visible = true;
                            manageBooks.Visible = true;
                            manageCourses.Visible = true;
                            manageInits.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
    }

}