﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BookStore._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <%--https://stackoverflow.com/questions/10872554/display-unknown-amount-of-images-from-database--%>
    <%--https://docs.microsoft.com/en-us/aspnet/web-forms/overview/getting-started/getting-started-with-aspnet-45-web-forms/display_data_items_and_details--%>

    <asp:ListView ID="productList" runat="server" DataKeyNames="postingID" GroupItemCount="4" ItemType="BookStore.Models.BookPosting" SelectMethod="GetProducts">
        <EmptyDataTemplate>
            <table>
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <EmptyItemTemplate>
            <td />
        </EmptyItemTemplate>
        <GroupTemplate>
            <tr id="itemPlaceholderContainer" runat="server">
                <td id="itemPlaceholder" runat="server"></td>
            </tr>
        </GroupTemplate>
        <ItemTemplate>
            <td runat="server">
                <table>
                    <tr>
                        <td>
                            <%--<a href="ProductDetails.aspx?productID=<%#:Item.postingID%>">--%>
                            <img src="Images/default.jpeg" runat="server" width="75" height="100" style="border: solid" /><%--</a>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="btnBuy" runat="server" OnClientClick = "return confirm('Congratulations You Bought This Book');"><h4><%#:Item.title%></h4></asp:LinkButton>
                            <%--<a href="ProductDetails.aspx?productID=<%#:Item.postingID%>"> should do this but wont yet--%>
                                <%--<span>
                                    <%#:Item.title%>
                                </span>--%>
                            <%--</a>--%>
                            <br />
                            <span>
                                <b>Price: </b><%#:String.Format("{0:c}", Item.price)%>
                            </span>
                            <br />
                            <span>
                                <b>Posting Date: </b><%#:Item.postingDate%>
                            </span>
                            <br />
                            <span>
                                <b>Author: </b><%#:Item.author%>
                            </span>
                            <br />
                            <span>
                                <b>ISBN: </b><%#:Item.ISBN%>
                            </span>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                </p>
            </td>
        </ItemTemplate>
        <LayoutTemplate>
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                <tr id="groupPlaceholder"></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr></tr>
                </tbody>
            </table>
        </LayoutTemplate>
    </asp:ListView>


    <div class="leftsidebar" style="border-style: solid; border-width: 1px; position: absolute; top: 80px; left: 5px; min-width: 250px; padding: 25px">
        <asp:TextBox ID="txtSearchBar" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />


        <asp:TreeView ID="tvSearchParams" runat="server" ExpandDepth="0">
            <Nodes>
                <asp:TreeNode ShowCheckBox="True" Text="Official" Value="Official"></asp:TreeNode>
                <asp:TreeNode ShowCheckBox="True" Text="Unofficial" Value="Unofficial"></asp:TreeNode>
                <asp:TreeNode ShowCheckBox="True" Text="School" Value="School">
                    <asp:TreeNode ShowCheckBox="True" Text="Conestoga College" Value="Conestoga College"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="University of Waterloo" Value="University of Waterloo"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Wilfrid Laurier" Value="Wilfrid Laurier"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode ShowCheckBox="True" Text="Subject" Value="Subject">
                    <asp:TreeNode ShowCheckBox="True" Text="All" Value="All"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="English" Value="English"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Math" Value="Math"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Science" Value="Science">
                        <asp:TreeNode ShowCheckBox="True" Text="Biology" Value="Biology"></asp:TreeNode>
                        <asp:TreeNode ShowCheckBox="True" Text="Chemistry" Value="Chemistry"></asp:TreeNode>
                        <asp:TreeNode ShowCheckBox="True" Text="Anatomy" Value="Anatomy"></asp:TreeNode>
                    </asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Engineering" Value="Engineering"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Arts" Value="Arts"></asp:TreeNode>
                    <asp:TreeNode></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode ShowCheckBox="True" Text="Book Condition" Value="Book Condition">
                    <asp:TreeNode ShowCheckBox="True" Text="Like New" Value="Like New"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Rebound" Value="Rebound"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Lightly Used" Value="Lightly Used"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Damaged" Value="Damaged"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="Heavily Used" Value="Heavily Used"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode ShowCheckBox="True" Text="Price Range" Value="Price Range">
                    <asp:TreeNode ShowCheckBox="True" Text="$0 - $20" Value="$0 - $20"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="$20 - $50" Value="$20 - $50"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="$50 - $100" Value="$50 - $100"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="$100 - $200" Value="$100 - $200"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="$200 - $500" Value="$200 - $500"></asp:TreeNode>
                    <asp:TreeNode ShowCheckBox="True" Text="$500+" Value="$500+"></asp:TreeNode>
                </asp:TreeNode>
            </Nodes>
        </asp:TreeView>
    </div>


</asp:Content>
