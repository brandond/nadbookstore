﻿/*
* FILE : User.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the definition for the Book data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Models
{
    public class Book
    {
        [Key]
        public string ISBN { get; set; }

        public string author { get; set; }

        public string title { get; set; }
    }
}
