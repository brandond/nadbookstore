﻿/*
* FILE : Register.aspx.cs
* PROJECT : PROG3080 - Nad Book Store
* PROGRAMMER : Nathan
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the back end code for the Register page
*/
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BookStore.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace BookStore.Account
{
    public partial class Register : Page
    {
        private string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
        /// <summary>
        /// Handles the Click event of the CreateUser control, registering a new user
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void CreateUser_Click(object sender, EventArgs e)
        {

            //check credentials over rest
            var requestURL = baseURL + @"/api/Users";
            var request = (HttpWebRequest)WebRequest.Create(requestURL);
            request.Method = "POST";

            if (Email.Text != "" && Password.Text != "" && FirstName.Text != ""  && LastName.Text != "")
            {

                User newuser = new User();
                newuser.userName = Email.Text;
                newuser.password = Password.Text;
                newuser.firstName = FirstName.Text;
                newuser.lastName = LastName.Text;
                newuser.accessLevel = 1;
                newuser.schoolID = 0;

                string body = JsonConvert.SerializeObject(newuser);

                request.ContentType = "application/json";
                var data = Encoding.ASCII.GetBytes(body);
                Stream newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);

                System.Web.HttpContext.Current.Session["UserName"] = Email.Text;
                System.Web.HttpContext.Current.Session["Password"] = Password.Text;

                IdentityResult result = IdentityResult.Failed();
                try
                {
                    var response = (HttpWebResponse)request.GetResponse();

                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
                    var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text };
                    result = manager.Create(user, Password.Text);

                    if (result.Succeeded)
                    {
                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        //string code = manager.GenerateEmailConfirmationToken(user.Id);
                        //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                        //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                        signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    }
                    else
                    {
                        ErrorMessage.Text = result.Errors.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    ErrorMessage.Text = "User already exists.";
                }
            }
            else
            {
                ErrorMessage.Text = "User not created.";
            }
        }
    }
}