﻿<%@ Page Title="MyListings" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyListings.aspx.cs" Inherits="BookStore.MyListings" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="wrapper">
        <div class="listingControls">
            <%--find some proper navigation controls or change look of buttons depending on page selected--%>
            <asp:Button ID="btnAddListing" class="ListingControl" runat="server" Text="Add Listing" OnClick="btnAddListing_Click" />            
            <br />
            <asp:Button ID="btnMyListings" class="ListingControl" runat="server" Text="My Listings" OnClick="btnMyListings_Click" />
            <br />
            <asp:Button ID="btnListingHistory" class="ListingControl" runat="server" Text="Listing History" OnClick="btnListingHistory_Click" />
            <br />
        </div>
        <div class="Listings">
            <asp:Panel ID="pnlAddListing" runat="server" Style="border-style: solid; border-width: 1px;">
                <table style="width:100%">
                    <tr>
                        <td rowspan="6"  style="width:1px">
                            <asp:Image ID="imgBook" runat="server"
                                AlternateText="Sample Book"
                                ImageAlign="left"
                                ImageUrl="Images/default.jpeg" Height="100px" Width="75px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:Label ID="lblBookTitle" runat="server" Text="Book Title:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBookTitle" runat="server"></asp:TextBox>
                        </td>                        
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:Label ID="lblISBN" runat="server" Text="ISBN:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                        </td>                        
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:Label ID="lblPrice" runat="server" Text="Price:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                        </td>                       
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:Label ID="lblAuthor" runat="server" Text="Author:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:Label ID="lblCondition" runat="server" Text="Condition:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCondition" runat="server">
                            <asp:ListItem>Like New</asp:ListItem>
                            <asp:ListItem>Rebound</asp:ListItem>
                            <asp:ListItem>Lightly Used</asp:ListItem>
                            <asp:ListItem>Damaged</asp:ListItem>
                            <asp:ListItem>Heavily Used</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:1px">
                            <asp:FileUpload ID="fuBookPic" runat="server" />
                            <br />
                        </td>
                        <td style="text-align:right" colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="Sumbit" align="right" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <!-- display users books -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- users books table -->
                    <asp:GridView ID="tblusersBooks" runat="server" EmptyDataText="No data available." Width="100%" CssClass="table table-striped table-bordered table-hover">
                    </asp:GridView>
                </div>
            </div>

            <!-- display users book history -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- users books table -->
                    <asp:GridView ID="tblbookHistory" runat="server" EmptyDataText="No data available." Width="100%" CssClass="table table-striped table-bordered table-hover">
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
