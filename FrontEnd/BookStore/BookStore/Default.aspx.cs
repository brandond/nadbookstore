﻿/*
* FILE : Default.aspx.cs
* PROJECT : PROG3080 - Nad Book Store
* PROGRAMMER : Nathan
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the back end code for the browsing page
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Newtonsoft.Json;
using BookStore.Models;

/*
* FILE			: Default.aspx.cs
* PROJECT		: PROG2030 -NAD - Final
* PROGRAMMER	: Nathan Nickel, Brandon Davies
* FIRST VERSION : 2017-01-05
* DESCRIPTION	: Browse book listings page
*/
namespace BookStore
{
/*
* NAME : _Default
* PURPOSE : Browse book listings and filter results
*/
    public partial class _Default : Page
    {
        // FUNCTION     : void Page_Load(object sender, EventArgs e)
        // DESCRIPTION  : check if user is logged in
        // PARAMETERS   : (object sender, EventArgs e)
        // RETURNS      : void
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
                var requestURL = baseURL + @"/api/Users/Login";
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                request.Method = "GET";


                var response = (HttpWebResponse)request.GetResponse();

                User user = null;

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Account/Login");
            }
            //get list of books
            var books = GetProducts();
        }

        /// <summary>
        /// Handles the Click event of the btnSearch control, updating the list based on the user search
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var bookName = txtSearchBar.Text;
            productList.DataSource = null;
            productList.Items.Clear();
            var books = GetProducts();
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns>the list of products</returns>
        public List<BookPosting> GetProducts()
        {       
            string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";

            var bookName = txtSearchBar.Text;
            var requestURL = baseURL + @"/api/postings/getProducts";
            if(bookName != "")
            {
                requestURL = baseURL + @"/api/postings/getProducts/" + bookName;
            }        
            var request = (HttpWebRequest)WebRequest.Create(requestURL);
            byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
            request.Method = "GET";

            var response = (HttpWebResponse)request.GetResponse();

            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    string content = reader.ReadToEnd();
                    var products = JsonConvert.DeserializeObject<List<BookPosting>>(content);

                    return products;
                }
            }
            
        }

        protected void btnBuy_Click(object sender, EventArgs e)
        {
            
        }
    }
}