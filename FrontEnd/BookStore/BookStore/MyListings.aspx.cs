﻿using BookStore.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookStore
{
    public partial class MyListings : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
                var requestURL = baseURL + @"/api/Users/Login";
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                request.Method = "GET";


                var response = (HttpWebResponse)request.GetResponse();

                User user = null;

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Account/Login");
            }
        }

        protected void btnAddListing_Click(object sender, EventArgs e)
        {
            btnAddListing.BackColor = System.Drawing.Color.Gray;
            btnMyListings.BackColor = System.Drawing.Color.Empty;
            btnListingHistory.BackColor = System.Drawing.Color.Empty;

            pnlAddListing.Visible = true;
            tblusersBooks.Visible = false;
            tblbookHistory.Visible = false;
        }

        protected void btnMyListings_Click(object sender, EventArgs e)
        {
            btnMyListings.BackColor = System.Drawing.Color.Gray;
            btnAddListing.BackColor = System.Drawing.Color.Empty;
            btnListingHistory.BackColor = System.Drawing.Color.Empty;

            pnlAddListing.Visible = false;
            tblusersBooks.Visible = true;
            tblbookHistory.Visible = false;
        }

        protected void btnListingHistory_Click(object sender, EventArgs e)
        {
            btnMyListings.BackColor = System.Drawing.Color.Empty;
            btnAddListing.BackColor = System.Drawing.Color.Empty;
            btnListingHistory.BackColor = System.Drawing.Color.Gray;

            pnlAddListing.Visible = false;
            tblusersBooks.Visible = false;
            tblbookHistory.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if(txtBookTitle.Text != "" && txtISBN.Text != "" && txtPrice.Text != "" && txtAuthor.Text != "" )
            {
                try
                {
                    string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";
                    //check credentials over rest
                    var requestURL = baseURL + @"/api/Postings/addProduct";
                    var request = (HttpWebRequest)WebRequest.Create(requestURL);
                    byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                    request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                    request.Method = "POST";

                    BookPosting newPosting = new BookPosting();
                    newPosting.userName = System.Web.HttpContext.Current.Session["UserName"] as String;
                    newPosting.title = txtBookTitle.Text;
                    newPosting.ISBN = txtISBN.Text;
                    newPosting.price = (float)Convert.ToDouble(txtPrice.Text);
                    newPosting.author = txtAuthor.Text;

                    string body = JsonConvert.SerializeObject(newPosting);

                    request.ContentType = "application/json";
                    var data = Encoding.ASCII.GetBytes(body);
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);

                    var response = (HttpWebResponse)request.GetResponse();

                    Response.Write("<script>alert('Listing was added');</script>");//happy message
                }
                catch(Exception ex)
                {
                    Response.Write("<script>alert('Error Listing could not be added. Review your listing and please try again');</script>");//error message
                    return;
                }
            }
            else
            {
                Response.Write("<script>alert('Error Listing could not be added. Review your listing and please try again');</script>");//error message
            }
        }
    }
}