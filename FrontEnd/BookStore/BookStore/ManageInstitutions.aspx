﻿<%@ Page Title="Manage Institutions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageInstitutions.aspx.cs" Inherits="BookStore.ManageInstitutions" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="panel-title">Users</h2>
    <asp:GridView ID="usersTable" runat="server" AllowSorting="true" EmptyDataText="No data available." Width="100%" CssClass="table table-striped table-bordered table-hover">
    </asp:GridView>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>


<!DOCTYPE html>
    <div>
    <asp:Repeater id="testDataGrid" runat="server" >

        <ItemTemplate>
            <table>
            <tr><td>Name</td><td><asp:TextBox runat="server" ID="CustomerName" Text= '<%# Eval("Name") %>' visible="true"/> </td>
            <td>ID</td><td><asp:TextBox runat="server" ID="Age" Text= '<%# Eval("ID") %>' visible="true" /></td>
            <td>Courses</td><td><asp:TextBox runat="server" ID="City" Text= '<%# Eval("Courses") %>' visible="true" /></td></tr>
                </table>
          </ItemTemplate>

    </asp:Repeater>
    </div>

</asp:Content>
