﻿/*
* FILE : Manageusers.aspx.cs
* PROJECT : PROG3080 - Nad Book Store
* PROGRAMMER : Nathan
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the back end code for the manage users page
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using BookStore.Models;
using System.Text;

namespace BookStore
{
    public partial class ManageUsers : Page
    {
        public string baseURL = @"http://nadbackenapi.us-west-2.elasticbeanstalk.com";

        /// <summary>
        /// Handles the Load event of the Page control, loads the list of users to the page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var requestURL = baseURL + @"/api/Users/Login";
                var request = (HttpWebRequest)WebRequest.Create(requestURL);
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
                request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
                request.Method = "GET";


                var response = (HttpWebResponse)request.GetResponse();

                User user = null;

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                        if (user.accessLevel < 2)
                        {
                            Response.Redirect("~/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Account/Login");
            }
        }

        public List<User> GetUsers()
        {
            var requestURL = baseURL + @"/api/Users";// /email
            var request = (HttpWebRequest)WebRequest.Create(requestURL);
            byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
            request.Method = "GET";

            var response = (HttpWebResponse)request.GetResponse();

            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    string content = reader.ReadToEnd();
                    var users = JsonConvert.DeserializeObject<List<User>>(content);

                    return users;
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            string userName = button.CommandName;

            var requestURL = baseURL + @"/api/Users/" + userName;// /email
            var request = (HttpWebRequest)WebRequest.Create(requestURL);
            byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
            request.Method = "DELETE";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                //deleted message
                Response.Redirect("~/ManageUsers");
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('User could not be deleted');</script>");//error message
            }
        }

        protected void ddlPermissonLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            string userName = ddl.DataTextField;
            string newPerm = ddl.SelectedItem.Value;

            var requestURL = baseURL + @"/api/Users/" + userName;// /email
            var request = (HttpWebRequest)WebRequest.Create(requestURL);
            byte[] byt = System.Text.Encoding.UTF8.GetBytes(System.Web.HttpContext.Current.Session["UserName"] as String + ":" + System.Web.HttpContext.Current.Session["Password"] as String);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
            request.Method = "GET";

            User user = null;
            var response = (HttpWebResponse)request.GetResponse();

            try
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string content = reader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(content);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('User could not be found, permission change failed');</script>");//error message user doesnt exist
                return;
            }

            user.accessLevel = Convert.ToInt32(newPerm);

            request = (HttpWebRequest)WebRequest.Create(requestURL);
            request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(byt));
            request.Method = "PUT";

            try
            {
                string body = JsonConvert.SerializeObject(user);
                request.ContentType = "application/json";
                var data = Encoding.ASCII.GetBytes(body);
                Stream newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('User could not be updated');</script>");//user not updated
            }
        }
    }
}