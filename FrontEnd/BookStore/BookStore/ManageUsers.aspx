﻿<%@ Page Title="Manage Users" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="BookStore.ManageUsers" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <br />
    <asp:ListView ID="productList" runat="server" DataKeyNames="userName" GroupItemCount="1" ItemType="BookStore.Models.User" SelectMethod="GetUsers">
        <EmptyDataTemplate>
            <table>
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <EmptyItemTemplate>
            <td />
        </EmptyItemTemplate>
        <GroupTemplate>
            <tr id="itemPlaceholderContainer" runat="server">
                <td id="itemPlaceholder" runat="server"></td>
            </tr>
        </GroupTemplate>
        <ItemTemplate>
            <td runat="server">
                <table style="width:100%; border:1px green solid;">
                    <tr>
                        <td colspan="42">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <span>
                                <b>First Name: </b><%#:Item.firstName%>
                            </span>
                        </td>
                        <td style="width: 15%;">
                            <span>
                                <b>Last Name: </b><%#:Item.firstName%>
                            </span>
                        </td>
                        <td style="width: 30%;">
                            <span>
                                <b>Email: </b><%#:Item.userName%>
                            </span>
                        </td>
                        <td style="width: 10%;">
                            <span>
                                <b>SchoolID: </b><%#:Item.schoolID%>
                            </span>
                        </td>
                        <td style="width: 20%;">
                            <span>
                                <b>Access Level</b>
                                <asp:DropDownList ID="ddlPermissonLevel" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPermissonLevel_SelectedIndexChanged" DataTextField=<%#:Item.userName%>>
                                    <asp:ListItem Value="1">Student</asp:ListItem>
                                    <asp:ListItem Value="2">Institution Faculty</asp:ListItem>
                                    <asp:ListItem Value="2">Institution Administrator</asp:ListItem>
                                    <asp:ListItem Value="3">System Administrator</asp:ListItem>
                                </asp:DropDownList>
                            </span>
                        </td>
                        <td style="width: 10%;">
                            <span>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" commandName=<%#:Item.userName%> />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="42">&nbsp;</td>
                    </tr>
                </table>
                </p>
            </td>
        </ItemTemplate>
        <LayoutTemplate>
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                <tr id="groupPlaceholder"></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr></tr>
                </tbody>
            </table>
        </LayoutTemplate>
    </asp:ListView>

</asp:Content>
