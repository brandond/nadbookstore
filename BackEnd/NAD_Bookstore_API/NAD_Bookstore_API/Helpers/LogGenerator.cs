﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NAD_Bookstore_API.Models;

namespace NAD_Bookstore_API.Helpers
{
    public class LogGenerator
    {
        public Log GenerateLog(string logType, string message)
        {
            var log = new Log();
            log.logTime = DateTime.Now.ToString("yyyy-mm-dd HH:mm");
            log.logType = logType;
            log.logMessage = message;
            return log;
        }

    }
}
