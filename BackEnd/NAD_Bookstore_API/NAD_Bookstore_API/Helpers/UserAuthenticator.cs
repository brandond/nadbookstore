﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCrypt.Net;
using NAD_Bookstore_API.Models;

namespace NAD_Bookstore_API.Helpers
{
    public class UserAuthenticator
    {
        private NadBookStoreBbContext _context;

        public UserAuthenticator(NadBookStoreBbContext context)
        {
            _context = context;
        }

        public User authenticateUser(string basicAuthHeader)
        {
            User result = null;
            try
            {
                if (basicAuthHeader != null && basicAuthHeader.StartsWith("Basic"))
                {
                    string encodedHeader = basicAuthHeader.Substring("Basic ".Length).Trim();
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedHeader));

                    int seperatorindex = usernamePassword.IndexOf(':');
                    var username = usernamePassword.Substring(0, seperatorindex);
                    var password = usernamePassword.Substring(seperatorindex + 1);


                    var hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
                    var user = _context.Users.FirstOrDefault(u => u.userName == username);

                    if (user != null)
                    {
                        if (BCrypt.Net.BCrypt.Verify(password, user.password))
                        {
                            result = new User();
                            result.accessLevel = user.accessLevel;
                            result.userName = user.userName;
                        }
                    }
                    if (result == null)
                    {
                        var log = new LogGenerator().GenerateLog("error", "Error: user" + username + "tried to login but failed, no user found with their credentials");
                        _context.Logs.Add(log);
                        _context.SaveChanges();
                    }
                }
                else
                {
                    var log = new LogGenerator().GenerateLog("error", "Error: no credentials were supplied when credentials were required");
                    _context.Logs.Add(log);
                    _context.SaveChanges();
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
