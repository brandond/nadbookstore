﻿/*
* FILE : User.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the definition for the Log data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace NAD_Bookstore_API.Models
{
    public class Log
    {
        [Key]
        public int logID { get; set; }

        public string logTime { get; set; }

        public string logType { get; set; }

        public string logMessage { get; set; }
    }
}
