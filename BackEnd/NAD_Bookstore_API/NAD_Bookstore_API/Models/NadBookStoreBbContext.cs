﻿/*
* FILE : NadBookStoreBdContext.cs
* PROJECT : PROG3080 - Nad Book Store
* PROGRAMMER : Brandon
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the definiton of the database context
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace NAD_Bookstore_API.Models
{
    public class NadBookStoreBbContext : DbContext
    {
        public NadBookStoreBbContext(DbContextOptions<NadBookStoreBbContext> options) : base(options)
        {

        }

        public DbSet<Book> Books { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<SchoolBook> School_Books { get; set; }

        public DbSet<School> Schools { get; set; }

        public DbSet<Posting> Postings { get; set; }

        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SchoolBook>()
                .HasKey(sb => new { sb.ISBN , sb.schoolID });
        }
    }
}
