﻿/*
* FILE : User.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the definition for the Posting data model
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NAD_Bookstore_API.Models
{
    public class Posting
    {
        [Key]
        public int postingID { get; set; }

        //fk
        public string userName { get; set; }

        //fk
        public string ISBN { get; set; }

        public float price { get; set; }

        public string postingDate { get; set; } //YYY-MM-DD

        public string expireDate { get; set; } //YYY-MM-DD

        public SByte active { get; set; }

        public SByte sold { get; set; }
    }
}
