﻿/*
* FILE : PostingController.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the code for the Posting routes, allowing for CRUD operations on the Postings table
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NAD_Bookstore_API.Models;
using NAD_Bookstore_API.Helpers;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NAD_Bookstore_API.Controllers
{
    public class bookProduct
    {
        public int postingID { get; set; }

        //fk
        public string userName { get; set; }

        public float price { get; set; }

        public string postingDate { get; set; } //YYY-MM-DD

        public string expireDate { get; set; } //YYY-MM-DD

        public SByte active { get; set; }

        public SByte sold { get; set; }

        public string author { get; set; }

        public string title { get; set; }

        public string ISBN { get; set; }
    }


    [Route("api/[controller]")]
    public class PostingsController : Controller
    {
        private readonly NadBookStoreBbContext _context;
        private Regex dateRegex = new Regex(@"^([0-9]){4}-([0-9]){2}-([0-9]){2}$");
        private UserAuthenticator authenticator;


        public PostingsController(NadBookStoreBbContext context)
        {
            _context = context;
            authenticator = new UserAuthenticator(context);
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        /// 
        [HttpGet]
        public IActionResult GetActive()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all postings, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }
   
            List<Posting> toRemove = new List<Posting>();
            foreach (var posting in _context.Postings)
            {
                if (Convert.ToBoolean(posting.sold) == true || Convert.ToBoolean(posting.active) == false || Convert.ToDateTime(posting.expireDate) < DateTime.Now)
                {
                    toRemove.Add(posting);
                }
            }

            foreach (var posting in toRemove)
            {
                _context.Postings.Remove(posting);
            }

            return new ObjectResult(_context.Postings.ToList());
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet("{id}", Name = "GetPosting")]
        public IActionResult GetAllById(long id)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all postings, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var posting = _context.Postings.FirstOrDefault(t => t.postingID == id);
            if (posting == null)
            {
                return NotFound();
            }
            return new ObjectResult(posting);
        }

        // GET api/postings/getProducts        
        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns>the list of products, or a web response indicating a failure during the process</returns>
        [HttpGet("getProducts")]
        public IActionResult getProducts()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all products, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var result = (from p in _context.Postings
                          join b in _context.Books on p.ISBN equals b.ISBN
                          select new bookProduct()
                          {
                              postingID = p.postingID,
                              userName = p.userName,
                              price = p.price,
                              postingDate = p.postingDate,
                              expireDate = p.expireDate,
                              active = p.active,
                              sold = p.sold,
                              ISBN = b.ISBN,
                              title = b.title,
                              author = b.author
                          });

            List<bookProduct> toRemove = new List<bookProduct>();

            var activeProducts = result.ToList();
            foreach (var posting in activeProducts)
            {
                if (Convert.ToBoolean(posting.sold) == true || Convert.ToBoolean(posting.active) == false || Convert.ToDateTime(posting.expireDate) < DateTime.Now)
                {
                    toRemove.Add(posting);
                }
            }

            foreach (var posting in toRemove)
            {
                activeProducts.Remove(posting);
            }
            return new ObjectResult(activeProducts);
        }

        // GET api/postings/getProducts/foobar        
        /// <summary>
        /// Gets the name of the products by book name.
        /// </summary>
        /// <param name="bookName">Name of the book.</param>
        /// <returns></returns>
        [HttpGet("getProducts/{bookName}")]
        public IActionResult getProductsByName(string bookName)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all postings by book name, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }
            var result = (from p in _context.Postings
                          join b in _context.Books on p.ISBN equals b.ISBN
                          select new bookProduct()
                          {
                              postingID = p.postingID,
                              userName = p.userName,
                              price = p.price,
                              postingDate = p.postingDate,
                              expireDate = p.expireDate,
                              active = p.active,
                              sold = p.sold,
                              ISBN = b.ISBN,
                              title = b.title,
                              author = b.author
                          });
            result = result.Where(p => p.title.Contains(bookName));
            List<bookProduct> toRemove = new List<bookProduct>();

            var activeProducts = result.ToList();
            foreach (var posting in activeProducts)
            {
                if (Convert.ToBoolean(posting.sold) == true || Convert.ToBoolean(posting.active) == false || Convert.ToDateTime(posting.expireDate) < DateTime.Now)
                {
                    toRemove.Add(posting);
                }
            }

            foreach (var posting in toRemove)
            {
                activeProducts.Remove(posting);
            }
            return new ObjectResult(activeProducts);
        }

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        [HttpPost("addProduct")]
        public IActionResult addProduct([FromBody] bookProduct product)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to add a product, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (product == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in product during creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid data specified in product");
            }

            var book = _context.Books.SingleOrDefault(b => b.ISBN == product.ISBN);
            if(book == null)
            {
                book = new Book();
                book.ISBN = product.ISBN;
                book.title = product.title;
                book.author = product.author;

                try
                {
                    _context.Books.Add(book);
                    _context.SaveChanges();
                }
                catch(Exception ex)
                {
                    var log = new LogGenerator().GenerateLog("error", ex.Message);

                    _context.Logs.Add(log);
                    _context.SaveChanges();
                    return BadRequest(ex.Message);
                }
            }

            var posting = new Posting();
            posting.ISBN = product.ISBN;
            posting.userName = product.userName;
            posting.price = product.price;
            posting.active = 1;
            posting.sold = 0;
            posting.postingDate = DateTime.Now.ToString("yyyy-MM-dd");

            var expireDate = DateTime.Now.AddDays(30);
            posting.expireDate = expireDate.ToString("yyyy-MM-dd");

            try
            {
                _context.Postings.Add(posting);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var log = new LogGenerator().GenerateLog("error", ex.Message);

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest(ex.Message);
            }

            var statusLog = new LogGenerator().GenerateLog("status", "A just added a product of with ISBN: "+ posting.ISBN+ " and username: "+ posting.userName);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();
            return new NoContentResult();
        }

        // POST api/values        
        /// <summary>
        /// Creates the specified posting.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>web response indicating the status of the creation</returns>
        [HttpPost]
        public IActionResult Create([FromBody] Posting item)
        {
            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data provided in Posting object to create");
            }
            else if (item.ISBN.ToString().Length != 13)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: ISBN must be 13 characters long");
            }
            else if(item.price <= 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: price must be grater than $0.00");
            }
            else if (item.postingDate.Length != 10 || !dateRegex.IsMatch(item.postingDate))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format for postingDate");
            }
            else if (item.expireDate.Length != 10 || !dateRegex.IsMatch(item.expireDate))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format for expireDate");
            }
            else if (item.active != 0 && item.active != 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: active must be 0 or 1");
            }
            else if (item.sold != 0 && item.sold != 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting creation");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: sold must be 0 or 1");
            }
            else
            {
                _context.Postings.Add(item);
                _context.SaveChanges();

                var statusLog = new LogGenerator().GenerateLog("status", "A just added a posting of with ISBN: " + item.ISBN + " and username: " + item.userName);

                _context.Logs.Add(statusLog);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // PUT api/values/5        
        /// <summary>
        /// Updates the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Posting item)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to update a product, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid data provided in Posting object to update");
            }
            else if (item.postingID != id)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: ID in the url must match the ID in the Posting object to update");
            }

            var posting = _context.Postings.FirstOrDefault(a => a.postingID == id);
            if (posting == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't find posting object to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data provided in Posting object to update");
            }
            else if (item.ISBN.ToString().Length != 13)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: ISBN must be 13 characters long");
            }
            else if (item.price <= 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: price must be grater than $0.00");
            }
            else if (item.postingDate.Length != 10 || !dateRegex.IsMatch(item.postingDate))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format for postingDate");
            }
            else if (item.expireDate.Length != 10 || !dateRegex.IsMatch(item.expireDate))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format for expireDate");
            }
            else if (item.active != 0 && item.active != 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: active must be 0 or 1");
            }
            else if (item.sold != 0 && item.sold != 0)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: sold must be 0 or 1");
            }
            else
            {
                posting.postingID = item.postingID;
                posting.userName = item.userName;
                posting.ISBN = item.ISBN;
                posting.price = item.price;
                posting.postingDate = item.postingDate;
                posting.expireDate = item.expireDate;
                posting.active = item.active;
                posting.sold = item.sold;

                _context.Postings.Update(posting);
                _context.SaveChanges();

                var statusLog = new LogGenerator().GenerateLog("status", "A just updated a product of with ISBN: " + posting.ISBN + " and username: " + posting.userName);

                _context.Logs.Add(statusLog);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to delete a product, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var posting = _context.Postings.FirstOrDefault(a => a.postingID == id);
            if (posting == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid data specified during posting update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            _context.Postings.Remove(posting);
            _context.SaveChanges();

            var statusLog = new LogGenerator().GenerateLog("status", "A just added a product of with ISBN: " + posting.ISBN + " and username: " + posting.userName);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
