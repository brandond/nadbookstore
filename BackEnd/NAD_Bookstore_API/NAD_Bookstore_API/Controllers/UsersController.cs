﻿/*
* FILE : UsersController.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the code for the User routes, allowing for CRUD operations on the Users table
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using NAD_Bookstore_API.Models;
using NAD_Bookstore_API.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NAD_Bookstore_API.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly NadBookStoreBbContext _context;
        private UserAuthenticator authenticator;
        private Regex emailRegex = new Regex(@"^\w+@\w+(\.\w+)+$");


        public UsersController(NadBookStoreBbContext context)
        {
            _context = context;
            authenticator = new UserAuthenticator(context);
        }

        // GET: api/values
        /// <summary>
        /// Gets all the users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<User> Get()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                return null;
            }
            return _context.Users.ToList();
        }

        // GET api/values/5
        
        /// <summary>
        /// Gets a user based on a specific username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
		[HttpGet("{userName}", Name = "GetUser")]
        public IActionResult GetById(string userName)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                return Unauthorized();
            }

            var item = _context.Users.SingleOrDefault(u => u.userName == userName);
            if(item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // GET api/Users/Login
        /// <summary>
        /// Logs in a user
        /// </summary>
        /// <returns></returns>
        [HttpGet("Login")]
        public IActionResult LoginUser()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                return Unauthorized();
            }
            return new ObjectResult(authorizedUser);
        }

        // POST api/values
        /// <summary>
        /// Creates a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromBody]User user)
        {
            if (user == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specifed for User during creation");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data specifed for User");
            }

            if (user.lastName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Last name cannot be long than 50 during user creation");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Last Name cannot be longer than 50 characters");
            }
            else if (user.firstName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: First name cannot be long than 50 during user creation");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: First Name cannot be longer than 50 characters");
            }
            else if (user.userName.Length > 50 || !emailRegex.IsMatch(user.userName))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid format of email during user creation");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format of email");
            }
            else
            {
                user.password = BCrypt.Net.BCrypt.HashPassword(user.password);

                try
                {
                    _context.Users.Add(user);
                    _context.SaveChanges();

                    var statusLog = new LogGenerator().GenerateLog("status", "A just added a user with username: " + user.userName);

                    _context.Logs.Add(statusLog);
                    _context.SaveChanges();
                }
                catch(Exception ex)
                {
                    var log = new LogGenerator().GenerateLog("error", ex.Message);
                    _context.Logs.Add(log);
                    _context.SaveChanges();
                    return BadRequest(ex.Message);
                }

                return new NoContentResult();
            }
        }

        // PUT api/values/5
        /// <summary>
        /// updates the specific user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut("{userName}")]
        public IActionResult Update(string userName, [FromBody]User user)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to log in to update users, and failed");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (user == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specifed during user update");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data specifed in User Object");
            }
            else if (user.userName != userName)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: mismatching userName specifed during user update between updated user and user url");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid user ID, please ensure that the ID you specify in the api url is the same ID used in the updated user");
            }

            if (user.accessLevel > authorizedUser.accessLevel)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to raise another user's access level to higher than their own");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var update = _context.Users.FirstOrDefault(u => u.userName == userName);
            if(update == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: couldn't locate user to update within the db");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            if (user.lastName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Last name cannot be long than 50 during user update");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Last Name cannot be longer than 50 characters");
            }
            else if (user.firstName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: First name cannot be long than 50 during user update");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: First Name cannot be longer than 50 characters");
            }
            else if (user.userName.Length > 50 || !emailRegex.IsMatch(user.userName))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: invalid format of email during user update");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid format of email");
            }
            else
            {
                update.firstName = user.firstName;
                update.lastName = user.lastName;
                update.accessLevel = user.accessLevel;
                update.schoolID = user.schoolID;

                _context.Users.Update(update);
                _context.SaveChanges();

                var statusLog = new LogGenerator().GenerateLog("status", "A just updated a user with username: " + user.userName);

                _context.Logs.Add(statusLog);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5
        
        /// <summary>
        /// deletes a specific user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
		[HttpDelete("{userName}")]
        public IActionResult Delete(string userName)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: User attempted to login to create a new user, but failed");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var user = _context.Users.FirstOrDefault(u => u.userName == userName);
            if(user == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate user to delete within the db");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            if (user.userName == authorizedUser.userName || user.accessLevel > authorizedUser.accessLevel)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: User attempted to either delete themselves, or attempted to delete a user with higher access level then themselves");
                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            _context.Users.Remove(user);
            _context.SaveChanges();

            var statusLog = new LogGenerator().GenerateLog("status", "A just deleted a user with username: " + user.userName);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
