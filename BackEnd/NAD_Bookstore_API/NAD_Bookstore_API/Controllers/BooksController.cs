﻿/*
* FILE : BooksController.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the code for the Book route, allowing for CRUD operations on the Books table
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using NAD_Bookstore_API.Models;
using NAD_Bookstore_API.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NAD_Bookstore_API.Controllers
{
    [Route("api/[controller]")]
    public class BooksController : Controller
    {
        private readonly NadBookStoreBbContext _context;

        public BooksController(NadBookStoreBbContext context)
        {
            _context = context;
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet]
        public IEnumerable<Book> GetAll()
        {
            return _context.Books.ToList();
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet("{id}", Name = "GetBook")]
        public IActionResult GetById(string id)
        {
            var item = _context.Books.FirstOrDefault(t => t.ISBN == id);
            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to locate a book by it's ISBN, but the book wasn't found");

                _context.Logs.Add(log);
                _context.SaveChanges();

                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values        
        /// <summary>
        /// Creates the specified Book.
        /// </summary>
        /// <param name="item">The Book.</param>
        /// <returns>http response indicating either success or fail of the creation</returns>
        [HttpPost]
        public IActionResult Create([FromBody] Book item)
        {
            if (item == null)
            {
                return BadRequest("Error: Invalid data provided in Cart object to create");
            }

            if (item.title.Length > 50)
            {
                return BadRequest("Error: title can't be longer than 50 characters");
            }
            else if (item.author.Length > 50)
            {
                return BadRequest("Error: author can't be longer than 50 characters");
            }
            else
            {
                _context.Books.Add(item);
                _context.SaveChanges();

                var statusLog = new LogGenerator().GenerateLog("status", "A just added a book with ISBN: " + item.ISBN);

                _context.Logs.Add(statusLog);
                _context.SaveChanges();

                return new NoContentResult();
            }
        }

        // PUT api/values/5        
        /// <summary>
        /// Updates the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] Book item)
        {
            if (item == null)
            {
                return BadRequest("Error: invalid data provided in Cart object to update");
            }
            else if (item.ISBN != id)
            {
                return BadRequest("Error: ID in the url must match the ID in the Book object to update");
            }

            var book = _context.Books.FirstOrDefault(a => a.ISBN == id);
            if (book == null)
            {
                return NotFound();
            }

            if (item.title.Length > 50)
            {
                return BadRequest("Error: title can't be longer than 50 characters");
            }
            else if (item.author.Length > 50)
            {
                return BadRequest("Error: author can't be longer than 50 characters");
            }
            else
            {
                book.ISBN = item.ISBN;
                book.author = item.author;
                book.title = item.title;

                _context.Books.Update(book);
                _context.SaveChanges();

                var statusLog = new LogGenerator().GenerateLog("status", "A just updated a book with ISBN: " + book.ISBN);

                _context.Logs.Add(statusLog);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var book = _context.Books.FirstOrDefault(a => a.ISBN == id);
            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            _context.SaveChanges();

            var statusLog = new LogGenerator().GenerateLog("status", "A just added a book with ISBN: " + book.ISBN);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();

            return new NoContentResult();
        }
    }
}
