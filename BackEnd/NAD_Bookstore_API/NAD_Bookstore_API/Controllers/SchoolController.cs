﻿/*
* FILE : SchoolController.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the code for the School routes, allowing for CRUD operations on the School table
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NAD_Bookstore_API.Models;
using NAD_Bookstore_API.Helpers;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NAD_Bookstore_API.Controllers
{
    [Route("api/[controller]")]
    public class SchoolController : Controller
    {
        private readonly NadBookStoreBbContext _context;
        private Regex emailDomailRegex = new Regex(@"^\w+.\w+$");
        private UserAuthenticator authenticator;

        public SchoolController(NadBookStoreBbContext context)
        {
            _context = context;
            authenticator = new UserAuthenticator(context);
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet]
        public IActionResult GetAll()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: User attempted to login to get all schools, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            return new ObjectResult(_context.Schools.ToList());
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the product by it's identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Either product searched for, or an Error depending on execution results</returns> 
        [HttpGet("{id}", Name = "GetSchool")]
        public IActionResult GetById(long id)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: User attempted to login to get a school by ID, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var school = _context.Schools.FirstOrDefault(t => t.schoolId == id);
            if (school == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate school object the user wanted to find");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }
            return new ObjectResult(school);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody] School item)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to create a school, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to create");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data provided in School object to create");
            }

            if (item.schoolName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to create");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: school Name can't be longer than 50 characters");
            }
            else if (item.emailDomain.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to create");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: author can't be longer than 50 characters");
            }
            else if (!emailDomailRegex.IsMatch(item.emailDomain))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to create");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: email domain name not valid");
            }
            else
            {
                _context.Schools.Add(item);
                _context.SaveChanges();

                return new NoContentResult();
            }
        }

        // PUT api/values/5        
        /// <summary>
        /// Updates the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] School item)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to update a school, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: invalid data provided in School object to update");
            }
            else if (item.schoolId != id)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: ID in the url must match the ID in the School object to update");
            }

            var school = _context.Schools.FirstOrDefault(a => a.schoolId == id);
            if (school == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate a school a user wanted to update.");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            if (item.schoolName.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: title can't be longer than 50 characters");
            }
            else if (item.emailDomain.Length > 50)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: author can't be longer than 50 characters");
            }
            else if (!emailDomailRegex.IsMatch(item.emailDomain))
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in school object user wanted to update");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: email domain name not valid");
            }
            else
            {
                school.schoolId = item.schoolId;
                school.schoolName = item.schoolName;
                school.emailDomain = item.emailDomain;

                _context.Schools.Update(school);
                _context.SaveChanges();
                return new NoContentResult();
            }
        }

        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product's identifier.</param>
        /// <returns>Either an OK status code, or an Error depending on execution results</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to delete a school, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var school = _context.Schools.FirstOrDefault(a => a.schoolId == id);
            if (school == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate a school a user wanted to delete");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            _context.Schools.Remove(school);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
