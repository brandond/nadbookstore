﻿/*
* FILE : SchoolBooksController.cs
* PROJECT : PROG2130 - Final Project
* PROGRAMMER : Brandon Davies, Hayden Taylor
* FIRST VERSION : 2018-01-01
* DESCRIPTION :
* This contains the code for the SchoolBook routes, allowing for CRUD operations on the SchoolBooks table
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NAD_Bookstore_API.Models;
using NAD_Bookstore_API.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NAD_Bookstore_API.Controllers
{
    [Route("api/[controller]")]
    public class SchoolBooksController : Controller
    {

        private readonly NadBookStoreBbContext _context;
        private UserAuthenticator authenticator;

        public SchoolBooksController(NadBookStoreBbContext context)
        {
            _context = context;
            authenticator = new UserAuthenticator(context);
        }

        // GET: api/values        
        /// <summary>
        /// Gets all the school books.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all school books, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            return new ObjectResult(_context.School_Books.ToList());
        }

        // GET api/values/5        
        /// <summary>
        /// Gets the school books by school identifier.
        /// </summary>
        /// <param name="schoolID">The school identifier.</param>
        /// <returns></returns>
        [HttpGet("{schoolID}")]
        public IActionResult GetBySchoolID(int schoolID)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get all school books by ID, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var item = _context.School_Books.Where(sb => sb.schoolID == schoolID).ToList();
            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate any school books the user wanted to find");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }
            return new ObjectResult(item);
        }

        /// <summary>
        /// Gets the specific school book.
        /// </summary>
        /// <param name="schoolID">The school identifier.</param>
        /// <param name="bookID">The book identifier.</param>
        /// <returns></returns>
        [HttpGet("{schoolID}/{bookID}")]
        public IActionResult GetSpecificSchoolBook(int schoolID, int bookID)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to get a school book, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var item = _context.School_Books.SingleOrDefault(sb => sb.schoolID == schoolID && sb.ISBN == bookID);
            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate a school book the user wanted to find");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values        
        /// <summary>
        /// Creates the specified school book.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromBody]SchoolBook sb)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to create a school books, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            if (sb == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Invalid data specified in schook book a user wanted to create");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return BadRequest("Error: Invalid data specified in SchoolBook object");
            }

            _context.School_Books.Add(sb);
            _context.SaveChanges();

            var statusLog = new LogGenerator().GenerateLog("status", "A just added a school book with ISBN: " + sb.ISBN + "and a schoolID of: " + sb.schoolID);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();

            return new NoContentResult();
        }


        // DELETE api/values/5        
        /// <summary>
        /// Deletes the specified school book.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(SchoolBook sb)
        {
            var authorizedUser = authenticator.authenticateUser(Request.Headers["Authorization"]);

            if (authorizedUser == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: user attempted to login to delete a school book, but failed");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return Unauthorized();
            }

            var item = _context.School_Books.FirstOrDefault(i => i.schoolID == sb.schoolID && i.ISBN == sb.ISBN);
            if (item == null)
            {
                var log = new LogGenerator().GenerateLog("error", "Error: Couldn't locate a school book the user wanted to delete");

                _context.Logs.Add(log);
                _context.SaveChanges();
                return NotFound();
            }

            _context.School_Books.Remove(item);
            _context.SaveChanges();

            var statusLog = new LogGenerator().GenerateLog("status", "A just deleted a school book with ISBN: " + item.ISBN + "and a schoolID of: " + item.schoolID);

            _context.Logs.Add(statusLog);
            _context.SaveChanges();

            return new NoContentResult();
        }
    }
}
